/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;



/**
 * This is MusicFile class
 * @author Toni
 */
public class MusicFile  implements MusicLibraryIF {

    private String artist;
    private String genre;
    private String name;
    private String album;
    private int trackNumber;
    private String path;
    private int id; 

    public  MusicFile(String artist, String name, String album, String genre, int trackNumber, String path) {
        this.album = album;
        this.artist = artist;
        this.name = name;
        this.genre = genre;
        this.trackNumber = trackNumber;
        this.path = path; 
   
    }
    
    public MusicFile(){
        
    } 
    
    @Override
    public String getArtist() {
        return artist;
    }

    public String getGenre() {
        return genre;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }

    public int getTrackNumber() {
        return trackNumber;
    }
    
    @Override
    public String getPath(){
        return path;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }
    
    @Override
    public String toString(){
        return name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    
}
