/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * DataAccessObject class
 *
 * Handles the connection with the database
 *
 * @author Toni
 */
public class MediaAccessObject implements MediaAccessObject_IF {

    private Connection myCon;

    /**
     * Opens the connection to the database.
     */
    public MediaAccessObject() { // konstruktori
        try {
            Class.forName("com.mysql.jdbc.Driver");
            myCon = DriverManager.getConnection("jdbc:mysql://localhost/multimediaplayer", "tonikuj", "tonikuj");
        } catch (Exception e) {
            System.err.println("Error while connecting to the database.");
            //System.exit(0);
        }
    }

    /**
     * Closes the connection to the database.
     */
    @Override
    protected void finalize() { 
        try { // the same connection throughout the application
            if (myCon != null) {
                myCon.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     /**
     * Adds musicfiles to the database.
     */

    @Override
    public boolean addNewMusicFileToDatabase(MusicFile file) {

     PreparedStatement myStatement = null;
        try { // runs the query and receive the result
            String Artist = file.getArtist(); 
            String Name = file.getName(); 
            String Album = file.getAlbum();
            int Tracknumber = file.getTrackNumber();
            String Genre = file.getGenre();
            String path = file.getPath();
            myStatement = myCon.prepareStatement("INSERT INTO musicfile (Album, Artist, Name, Genre, Pathlocation,Tracknumber ) VALUES (?, ?, ?, ?, ?, ?)");
            myStatement.setString(1, Album);
            myStatement.setString(2, Artist);
            myStatement.setString(3, Name);
            myStatement.setString(4, Genre);
            myStatement.setString(5, path);
            myStatement.setInt(6, Tracknumber); 
            myStatement.executeUpdate();
            
            return true; 
            
            } catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (myStatement != null)
					myStatement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return false;
	}

    @Override
    public boolean addNewVideoFileToDatabase(HashMap fileMetadata) {
        return false;
    }
    
    
    public String getMusicFilefromDatabase(String Name) {
		return null;
	}
    
    /**
    * Gets all musicfiles from the database.
     * @return 
    */
    @Override
    public MusicFile[] getAllMusicFilesFromDatabase() {
        ArrayList<MusicFile> list =  new ArrayList<MusicFile>();
            Statement myStatement = null;
            ResultSet myRs = null;
            try { // runs the query and receive the result
                    myStatement = myCon.createStatement();
                    String sqlSelect = "SELECT * FROM musicfile";
                    myRs = myStatement.executeQuery(sqlSelect);
                    while (myRs.next()){
                            String Artist = myRs.getString("artist");
                            String Name = myRs.getString("name");
                            String Album = myRs.getString("album");
                            String Genre = myRs.getString("genre"); 
                            int Tracknumber =myRs.getInt("tracknumber");
                            String path = myRs.getString("pathlocation"); 
                            int id = myRs.getInt("musicid"); 
                            MusicFile music = new MusicFile(); 
                            music.setArtist(Artist);
                            music.setName(Name);
                            music.setAlbum(Album);
                            music.setGenre(Genre);
                            music.setTrackNumber(Tracknumber);
                            music.setPath(path);
                            music.setId(id);
                            list.add(music); 
                            
                             
                    }

            } catch (Exception e) {
                    e.printStackTrace();
            } finally {
                    try {
                            if (myRs != null)
                                    myRs.close();
                            if (myStatement != null)
                                    myStatement.close();
                    } catch (Exception e) {
                            e.printStackTrace();
                    }
            }
            MusicFile[] returnArray = new MusicFile[list.size()];
            return (MusicFile[])list.toArray(returnArray);
            
            
    }
    @Override
    public boolean getAllVideoFilesFromDatabase() {
        return false;
    }

    @Override
    public boolean updateMusicFileById(int id) {
        return false;
    }

    @Override
    public boolean updateVideoFileById(int id) {
        return false;
    }

    @Override
    public boolean removeMusicFileById(int id) {
        return false;
    }

    @Override
    public boolean removeVideoFileById(int id) {
        return false;
    }

}
