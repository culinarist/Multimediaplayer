/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

/**
 *
 * @author Toni
 */
public interface MusicLibraryIF {
    
    public String getName();
    public String getArtist();
    public String getPath();
    
}
