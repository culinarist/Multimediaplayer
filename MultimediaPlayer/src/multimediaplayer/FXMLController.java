/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeView;

/**
 * FXML Controller class
 *
 * @author Toni, Sami
 */
public class FXMLController implements Initializable, FXMLController_IF {

    private FXMLModel_IF model;

    @FXML
    public TreeView musicLibraryTreeView;
    @FXML
    public TreeView videoLibraryTreeView;

    @FXML
    public Tab musicLibrary;
    @FXML
    public Tab videoLibrary;
    @FXML
    public Button playButton;
    @FXML
    public Button pauseButton;
    @FXML
    public Label songName;
    @FXML
    public Label search;
    @FXML
    public Label search2;
    @FXML
    public Label artist;
    @FXML
    public Menu file;
    @FXML
    public Menu edit;
    @FXML
    public Menu help;
    @FXML
    public MenuItem close;
    @FXML
    public MenuItem addNewMusicFile;
    @FXML
    public MenuItem addNewVideoFile;
    @FXML
    public MenuItem createPlaylist;
    @FXML
    public MenuItem changeLanguage;
    @FXML
    public MenuItem about;
    @FXML
    public Button previous;
    @FXML
    public Button next;

    public static ResourceBundle messages;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        model = new FXMLModel(this);
        populateMusicLibrary();
        populateVideoLibrary();

        Locale currentLocale = new Locale("en", "US");
        messages = ResourceBundle.getBundle("multimediaplayer.MessagesBundle", currentLocale);
        changeLanguage();
    }

    /**
     * Gives model the order to add music file
     */
    @FXML
    @Override
    public void addNewMusicFile() {
        model.addNewMusicFile();
    }

    /**
     * Gives model the order to add video file
     */
    @FXML
    @Override
    public void addNewVideoFile() {
        model.addNewVideoFile();
    }

    @FXML
    @Override
    public void populateMusicLibrary() {
        model.populateMusicLibrary(musicLibraryTreeView);
    }

    @FXML
    @Override
    public void populateVideoLibrary() {
        model.populateVideoLibrary(videoLibraryTreeView);
    }

    @Override
    public void playSelectedMusicFile() {
        model.playSelectedMusicFile();
    }

    @Override
    public void pauseMusicFile() {
        model.pauseMusicFile();
    }

    @Override
    public void close() {
        System.exit(0);
    }

    @Override
    public void changeLanguage() {
        musicLibrary.setText(messages.getString("musicLibrary"));
        videoLibrary.setText(messages.getString("videoLibrary"));
        playButton.setText(messages.getString("play"));
        pauseButton.setText(messages.getString("pause"));
        artist.setText(messages.getString("artist"));
        songName.setText(messages.getString("songName"));
        search.setText(messages.getString("search"));
        search2.setText(messages.getString("search"));
        file.setText(messages.getString("file"));
        edit.setText(messages.getString("edit"));
        help.setText(messages.getString("help"));
        close.setText(messages.getString("close"));
        addNewMusicFile.setText(messages.getString("addNewMusicFile"));
        addNewVideoFile.setText(messages.getString("addNewVideoFile"));
        createPlaylist.setText(messages.getString("createPlaylist"));
        changeLanguage.setText(messages.getString("changeLanguage"));
        about.setText(messages.getString("about"));
        previous.setText(messages.getString("previous"));
        next.setText(messages.getString("next"));
    }

    @Override
    public void selectSwedish() {
        Locale currentLocale = new Locale("sv", "SE");
        messages = ResourceBundle.getBundle("multimediaplayer.MessagesBundle", currentLocale);
        changeLanguage();
    }

    @Override
    public void selectEnglish() {
        Locale currentLocale = new Locale("en", "US");
        messages = ResourceBundle.getBundle("multimediaplayer.MessagesBundle", currentLocale);
        changeLanguage();
    }
}
