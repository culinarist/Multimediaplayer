/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import javafx.scene.control.TreeView;

/**
 * Interface for the FXMLModel class
 * 
 * @author Toni
 */
public interface FXMLModel_IF {
    public abstract void addNewMusicFile();
    public abstract void addNewVideoFile();
    public abstract void populateMusicLibrary(TreeView tree);
    public abstract void populateVideoLibrary(TreeView tree);
    public abstract void playSelectedMusicFile();
    public abstract void pauseMusicFile();
    public abstract void setPlayingSongText(String name);
    public abstract void setPlayingArtistText(String name);
}
