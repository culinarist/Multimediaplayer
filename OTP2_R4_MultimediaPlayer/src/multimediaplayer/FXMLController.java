/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 * 
 * @author Toni, Sami
 */
public class FXMLController implements Initializable, FXMLController_IF {
    
    FXMLModel_IF model;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        model = new FXMLModel();
    }
    
    /**
     * Gives model the order to add music file
     */
    @FXML
    @Override
    public void addNewMusicFile() {
        model.addNewMusicFile();
    }
    
    /**
     * Gives model the order to add video file
     */
    @FXML
    @Override
    public void addNewVideoFile() {
        model.addNewVideoFile();
    }
    
    
}
