/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.io.File;
import java.util.List;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.jaudiotagger.audio.mp3.MP3File;

/**
 * FXML Model class
 *
 * @author Toni
 */
public class FXMLModel implements FXMLModel_IF {
    
    //MediaAccessObject mao = new MediaAccessObject();
    
    /**
     * Opens a filechooser where the user can choose music files to be added to the library
     * (not ready yet)
     */
    @Override
    public void addNewMusicFile() {
        FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select music files");
		fileChooser.getExtensionFilters().addAll(
			//new FileChooser.ExtensionFilter("Music Files", "*.wav", "*.mp3", "*.mp4", "*.m4a", "*.flac", "*.wma"));
                        new FileChooser.ExtensionFilter("Music Files", "*.mp3"));
		List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        
        if (selectedFiles != null) {
            for (int i = 0; i < selectedFiles.size(); i++){
                File file = selectedFiles.get(i);
            }
	}
	else {
            
	}
    }
    
    /**
     * Opens a filechooser where the user can choose video files to be added to the library
     * (not ready yet)
     */
    @Override
    public void addNewVideoFile() {
        FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select video files");
		fileChooser.getExtensionFilters().addAll(
			new ExtensionFilter("Video Files", "*.avi", "*.mkv", "*.mpeg-4", "*.mov", "*.asf"));
		List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        
        if (selectedFiles != null) {
            
	}
	else {
            
	}
    }
    
}
